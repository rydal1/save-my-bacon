KEEP_FOR_DAYS=1
while read line; do
  if echo $line | grep -qv '^#'./ >/dev/null; then
    varname=$(echo "$line" | cut -d '=' -f 1)
    varvalue=$(echo "$line" | cut -d '=' -f 2)

    if [ "$varname" == "keep_for_days" ]; then
      re='^[0-9]+$'
      if ! [[ $varvalue =~ $re ]]; then
        echo "Error: warning_size_limit in /etc/recycled.conf not a number" >&2
        exit 6
      fi
      KEEP_FOR_DAYS="$varvalue"
    fi
  fi
done </etc/recycled.conf
if [ -f "/recycled/" ] ; then
  find /recycled -mtime +"$KEEP_FOR_DAYS" -exec rm {} \;
else
   true
fi
