# save-my-bacon
<b>A shell script which could save your career :)</b>

<p>Every System administrator's nightmare is wiping a system, and I'm sure we've all panicked whilst looking for backups.</p>
<p>It would be nice to have a recycle bin not only for the user interface but the shell as well.</p>
<p>That's what I've written in bash, as a debian and redhat package as well as in this git repo.
<p>Deleting more than a gigabyte of stuff? If you are it asks for confirmation, this value can be set in the config file, at /etc/recycled.conf</p>
<ul>
<li>Won't it conflict with my scripts? No it won't as it's aliased in your ~/.bashrc file, it only applies when in the shell.</li>
<li>Files to be removed are stored in /recycled/UID/timestamp</li>
<li>The user name is symlinked to the user id as the user id does not change</li>
<li>It covers files replaced by the delete (rm), move (mv) and copy(cp) commands</li>
<li>To ignore it's use, pass -z and it will be bypassed. </li>
<li>It adds a cron entry in /etc/cron.daily to purge items in the recycled folder older than 2 days, or as set in the configuration file</li>
</ul>
<p>The /etc/recycled.conf file contains the settings:</p>
<ul>
<li>
warning_size_limit=1000000 // warn if deleting / replacing more than n kilobytes of data. This removes the backup facility of this script.
</li>
<li>
interactive_mode="true" /// Force -i on each command run to ask before overwriting, good when trying this script.
</li>
<li>
keep_for_days=2 // A cronjob will delete files from the /recycled folder older than n days as set by this command.
</li>
</ul>
<p>To use as this gitlab repository please copy the recycled.conf file to /etc, the purge_old_recycled_files.sh to /etc/cron.daily and the save-my-bacon file to /usr/bin, or use our debian and redhat packages as listed below:</p>
<br>
Packages and a video walkthrough at our <a href="https://www.rydalinc.org/bacon.php">Rydalinc website.</a>

<p>Accepting pull requests</p>

<a href="https://www.rydalinc.org/index.php">Check our other amazing projects.</a>
<hr>
<p><b>Finally</b></p>I wrote <a href="https://github.com/rydal/dissertation-doc"> Tuxconfig</a> a while ago, as my dissertation at Kent university for my course in Advanced Computer science. It's a Linux based version of the Windows device installer Wizard.</p>

<p>That was three years ago.</p>  
<p>Now my coding skills have improved remarkably, to the point I've written:</p>
<p><A href="https://www.bitflash.me">Bitflash</A>, a notification daemon which keeps your anonymity.</p>
<p><A href="https://likeme.mx"> Likeme.mx</a>, a web based contact sharing app using QR codes.
<p><A href="https://www.nextplease.live">Next!</a> A virtual queue display with less aggravation to staff and customers.</p>
<br>
<p>Anyway seeing as my portfolio proves I can write quality software I'm thinking of relaunching my dissertation in python with several modifications. If you are Linux foundation, Ubuntu or an open source player and wish to sponsor it please get in touch at <A href="mailto:rob@rydalinc.org">rob@rydalinc.org</a>
<br>
<p><b>Thanks for reading,</b></p>
<p> Roblet</p>
